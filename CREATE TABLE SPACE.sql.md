
# COMMANDE CRÉATION DE TABLE

CREATE SMALLFILE TABLESPACE applicationTables
DATAFILE 'C:\Oracle\oradata\b55\appTable01.dbf' SIZE 150M REUSE AUTOEXTEND ON NEXT 250M
MAXSIZE 1G
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
DEFAULT COMPRESS
LOGGING 
FLASHBACK ON
ONLINE;


# EXPLICATION

#CREATE SMALLFILE TABLESPACE applicationTables: 
> Créer un table space appelé applicationTables

#AUTOEXTEND ON NEXT
> Extends toi jusqu'à 250MB si tu dépasse créé un nouveau fichier


#MAXSIZE 1G
>La taille maximum du tablespace est de 1G

#EXTENT MANAGEMENT LOCAL AUTOALLOCATE
> Means that the extent sizes are managed by Oracle


#SEGMENT SPACE MANAGEMENT AUTO
> Gère les segments en utilisant le bitmap


#LOGGING 
> mode de journalisation logging == on journalise


#FLASHBACK ON
>Pour pouvoir revenir en arrière (à un certain SCN). Est-ce que je veux pouvoir revenir en arrière à un moment donné

#ONLINE
> On peut avoir accès à la tablespace





